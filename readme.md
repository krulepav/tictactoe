# Tic tac toe tournament

Webová hra Turnaj v tic-tac-toe. Aplikace vznikla jako semestrální práce pro předmět KAJ.

## Popis
Hráči se na začátku zaregistrují svým uživatelským jménem a heslem. Následně jeden z nich zahájí nový turnaj a vybere hráče, kteří se nového turnaje zúčastní.  
Aplikace sama náhodně sestavuje dvojice hráčů, kteří spolu budou hrát úvodní kola. Následně se hraje systémem "pavouk", tedy vítěz postupuje do dalšího kola, kde se utká s vítězem jiného kola, dokud není znám celkový vítěz.  

Hráči mimo jiné za každé kolo dostávají body - 3 za vítězství, 1 za remízu. Na základě těchto bodů se sestavuje žebříček TOP 10.

Veškerá data se ukládají do LocalStorage. Pokud se někdo pokusí upravit si skóre, systém to pozná a hráči nebude umožněno hrát, dokud skóre nevrátí na původní hodnotu.

Díky absenci backendu je aplikaci možno hrát i offline.


## Set up
Aplikace je psána v TypeScriptu s využitím několika NPM balíčků. Před spuštěním je tedy nejprve nutné mít nainstalované NPM a provést příkaz ``$npm install``. Následně již můžeme spustit program tsc ve watch módu, který bude automaticky kompilovat TypeScript do JavaScriptu ``$npm run tsc``. Jakmile zapneme webový server ``$npm start``, aplikace bude dostupná na http://localhost:3000

## Body zadání, které byly implementovány
Bod hodnocení|Popis|Je v semestrálce
---|---|---
Validita |Validní použití HTML5 doctype | ANO
Cross-browser kompatibilita |Fungující v moderních prohlíčečích v posledních vývojových verzích (Chrome, Firefox, Edge, Opera) | ANO
Semantické značky |správné použití sémantických značek (section, article, nav, aside, ...) | ANO
Grafika - SVG / Canvas||ANO - SVG
Média - Audio/Video||ANO - audio při hře
Formulářové prvky |Validace, typy, placeholder, autofocus| ANO
Offline aplikace| využití možnosti fungování stránky bez Internetového připojení (viz sekce Javascript) |ANO - cachce manifest
Pokročilé selektory| použití pokročilých pseudotříd a kombinátorů| ANO
Vendor prefixy || NE
CSS3 transformace 2D/3D|| ANO - 2D zoom checkboxu (aby byl větší)
CSS3 transitions/animations ||ANO - Barvy položek menu
Media queries| stránky fungují i na mobilních zařízeních i jiných (tedy nerozpadají se) | ANO
OOP přístup| prototypová dědičnost, její využití, jmenné prostory | ANO
Použití JS frameworku či knihovny| použití a pochopení frameworku či knihovny JAK, jQuery, .. |ANO - jQuery, TypeScript
Použití pokročilých JS API |využití pokročilých API (File API, Geolocation, Drag & Drop, LocalStorage, Sockety, ...)|ANO - LocalStorage
Funkční historie| posun tlačítky zpět/vpřed prohlížeče - pokud to vyplývá z funkcionatilty (History API) | ANO
Ovládání medií |použití Média API (video, zvuk), přehrávání z JS | ANO - audio při hře
Offline aplikace| využití JS API pro zjišťování stavu | NE 
JS práce se SVG| události, tvorba, úpravy| ANO - při hře

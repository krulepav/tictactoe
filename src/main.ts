import registerHandler from "./controller/RegisterController";
import loginHandler from "./controller/LoginController";
import SPAController from "./controller/SPAController";
import {GameController} from "./controller/GameController";
import NewTournamentController from "./controller/NewTournamentController";
import HighscoresController from "./controller/HighscoresController";
import TournamentController from "./controller/TournamentController";
import TournamentService from "./service/TournamentService";

//Do not delete this statements
//otherwise decorators will not
//be initialised
HighscoresController;
NewTournamentController;
TournamentController;
GameController;

SPAController.init("home");
TournamentService.checkTournamentInProgress();

$("#register-form").submit(registerHandler);
$("#login-form").submit(loginHandler);
$("#new-tournament-form").submit(NewTournamentController.onNewTournamentSubmit);
$("#highscores-table-btn-select-all").click(NewTournamentController.selectAll);
$("#highscores-table-btn-unselect-all").click(NewTournamentController.unselectAll);
$("#highscores-table-btn-clear-filter").click(NewTournamentController.clearFilter);
$("#players-game-login").submit(GameController.startNewGameHandler);

$("#new-tournament-players-filter").keyup((e) => {
    NewTournamentController.filterPlayerList($(e.target).val());
});

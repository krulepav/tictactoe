//Polyfill to allow Storage save and load JS objects

Storage.prototype.setObject = function (key, object) {
    localStorage.setItem(key, JSON.stringify(object));
};

Storage.prototype.getObject = function (key) {
    return JSON.parse(localStorage.getItem(key))
};

interface Storage {
    setObject(key: string, object: any): void;
    getObject(key: string): any;
}
import Tournament from "../model/Tournament";
import TournamentDAO from "../dao/TournamentDAO";
import Contest from "../model/Contest";
import ContestLayer from "../model/ContestLayer";
export default class TournamentService {
    static getLastTournament(): Tournament {
        return TournamentDAO.getTournament(TournamentDAO.getNumberOfTournaments());
    }

    static makeTournament(playersUsernames: string[]): Tournament {
        let contestLayer = new ContestLayer();
        while (playersUsernames.length > 0) {
            let first: string = playersUsernames[Math.floor(Math.random() * playersUsernames.length)];
            playersUsernames.splice(playersUsernames.indexOf(first), 1);

            let second: string = playersUsernames[Math.floor(Math.random() * playersUsernames.length)];
            playersUsernames.splice(playersUsernames.indexOf(second), 1);

            contestLayer.contests.push(new Contest(first, second));
        }
        let tournament = new Tournament(TournamentService.nextTournamentId());
        tournament.contestLayers[0] = contestLayer;
        return tournament;
    }

    static getNextContest(tournament: Tournament): Contest {
        if (tournament == null) {
            return null;
        }

        //if the last layer hasn't been finished
        for (let i = 0; i < tournament.contestLayers.length; i++) {
            let layer = tournament.contestLayers[i];
            for (let j = 0; j < layer.contests.length; j++) {
                let contest = layer.contests[j];
                if (contest.winnerUser == null) {
                    return contest;
                }
            }
        }

        let lastLayer = tournament.contestLayers[tournament.contestLayers.length - 1];

        //if the last layer has total winner
        if (lastLayer.contests.length <= 1) {
            tournament.finished = true;
            TournamentDAO.updateTournament(tournament);
            return null;
        }

        //if all the layers have been finished but no total winner make new layer
        let newLayer = new ContestLayer();
        for (let i = 0; i < lastLayer.contests.length; i += 2) {
            let contest = new Contest(lastLayer.contests[i].winnerUser, lastLayer.contests[i + 1].winnerUser);
            newLayer.contests.push(contest)
        }
        tournament.contestLayers.push(newLayer);
        TournamentDAO.updateTournament(tournament);
        return newLayer.contests[0];
    }

    /**
     * Returns last used ID in DB plus one
     * @returns {number}
     */
    static nextTournamentId(): number {
        return TournamentDAO.getNumberOfTournaments() + 1;
    }

    static checkTournamentInProgress() {
        if (TournamentService.getNextContest(TournamentDAO.getLastTournament()) == null) {
            $("nav a[href='#game']").hide();
            $("nav a[href='#new-tournament']").show();
        } else {
            $("nav a[href='#game']").show();
            $("nav a[href='#new-tournament']").hide();
        }
    }
}

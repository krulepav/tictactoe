import ContestLayer from "./ContestLayer";

export default class Tournament {
    contestLayers: ContestLayer[] = [];
    finished: boolean = false;

    constructor(readonly id: number) {
    }
}
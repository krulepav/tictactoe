export default class Contest {
    constructor(readonly usernameCrosses: string, readonly usernameCircles: string, public winnerUser: string = null) {
    }
}
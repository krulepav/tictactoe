export class User {
    constructor(readonly userName: string, readonly password: string, public score: number = 0) {
    }
}
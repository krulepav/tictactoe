import Tournament from "../model/Tournament";
const STORAGE_KEY_TOURNAMENTS = "tournaments";
export default class TournamentDAO {
    static getNumberOfTournaments(): number {
        return parseInt(localStorage.getItem(STORAGE_KEY_TOURNAMENTS + "_count")) || 0;
    }

    /**
     * Save tournament into local storage
     * @param tournament Tournament to be saved
     * @returns {boolean} True if tournament was saved, false otherwise
     */
    static saveTournament(tournament: Tournament): boolean {
        let numOfTournaments = TournamentDAO.getNumberOfTournaments();
        localStorage.setItem(STORAGE_KEY_TOURNAMENTS + "_count",
            (numOfTournaments + 1).toString());

        TournamentDAO.updateTournament(tournament);
        return true;
    }

    /**
     * Return all tournaments in local storage
     * @returns {Tournament[]} Array of all saved tournaments
     */
    static getTournaments(): Tournament[] {
        let tournaments: Tournament[] = [];
        for (let i = 1; i <= TournamentDAO.getNumberOfTournaments(); i++) {
            tournaments.push(localStorage.getObject(STORAGE_KEY_TOURNAMENTS + "_" + i));

        }
        return tournaments;
    }

    static getLastTournament() {
        return TournamentDAO.getNumberOfTournaments() > 0 ?
            TournamentDAO.getTournament(TournamentDAO.getNumberOfTournaments()) : null;
    }


    /**
     * Update (replace) tournament with same tournamentName with new object
     * @param tournament New tournament instance that will replace existing tournament with same tournamentName
     */
    static updateTournament(tournament: Tournament) {
        localStorage.setObject(STORAGE_KEY_TOURNAMENTS + "_" + tournament.id, tournament);
    }

    /**
     * Load tournament with given tournament id
     * @param id
     * @returns {Tournament|null} Tournament or null if tournament id does not exist
     */
    static getTournament(id: number): Tournament {
        return localStorage.getObject(STORAGE_KEY_TOURNAMENTS + "_" + id);
    }
}
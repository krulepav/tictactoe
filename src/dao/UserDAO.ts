import {User} from "../model/User";

const STORAGE_KEY_USERS = "users";
export default class UserDAO {

    /**
     * Save user into local storage
     * @param user User to be saved
     * @returns {boolean} True if user was saved, false otherwise (Username already existed)
     */
    static saveUser(user: User): boolean {
        let currentUserWrappers: UserWrapper[] = UserDAO.getUserWrappers() || [];
        //Prevent duplicated username
        for (let userWrapper of currentUserWrappers) {
            if (userWrapper.userName == user.userName)
                return false;
        }
        currentUserWrappers.push(new UserWrapper(user));
        localStorage.setObject(STORAGE_KEY_USERS, currentUserWrappers);
        return true;
    }

    /**
     * Return all users in local storage
     * @returns {User[]} Array of all saved users
     */
    static getUserWrappers(): UserWrapper[] {
        return localStorage.getObject(STORAGE_KEY_USERS) || [];
    }

    /**
     * Update (replace) user with same userName with new object
     * @param user New user instance that will replace existing user with same userName
     */
    static updateUser(user: User) {
        let currentUserWrappers: UserWrapper[] = UserDAO.getUserWrappers() || [];

        for (let i = 0; i < currentUserWrappers.length; i++) {
            if (currentUserWrappers[i].userName === user.userName) {
                currentUserWrappers[i] = new UserWrapper(user);
                break;
            }
        }
        localStorage.setObject(STORAGE_KEY_USERS, currentUserWrappers);
    }

    /**
     * Load user with given userName and password
     * @param userName
     * @param password
     * @returns {User|null} User or null if username does not exist or wrong password
     */
    static getUser(userName: string, password: string): User {
        let userWrappers = UserDAO.getUserWrappers();
        let userWrapper: UserWrapper = (userWrappers && userWrappers.filter((user) => {
            return userName == user.userName;
        })[0]);
        if (!userWrapper)
            return null;

        let user: User = UserWrapper.getUserFromUserWrapper(userWrapper, password);
        if (user && user.password == password)
            return user;
        else
            return null;
    }
}

class UserWrapper {
    readonly userName: string;
    readonly score: number;
    readonly encryptedUserJSON: string;

    constructor(user: User) {
        this.userName = user.userName;
        this.score = user.score;
        this.encryptedUserJSON = UserWrapper.xorEncode(JSON.stringify(user), user.password);
    }

    static getUserFromUserWrapper(userWrapper: UserWrapper, password: string): User {
        let result: any;
        try {
            result = JSON.parse(UserWrapper.xorEncode(userWrapper.encryptedUserJSON, password));
        } catch (err) {
            return null;
        }

        if (result && result.password && result.password == password) {
            if (result.score != userWrapper.score) {
                let error = `It looks like someone changed score from ${result.score} to ${userWrapper.score}`;
                alert(`CHYBA: Uživateli ${userWrapper.userName} někdo změnil skóre z ${result.score} na ${userWrapper.score}`);
                throw new Error(error);
            }
            return new User(result.userName, result.password, result.score);
        }
        else {
            return null;
        }
    }

    //based on http://snipplr.com/view/46795/
    private static xorEncode(value: string, password: string): string {
        let ord: number[] = [];
        let buf = "";
        let z: number;

        for (z = 1; z <= 255; z++) {
            ord[<any>String.fromCharCode(z)] = z
        }

        for (let j = z = 0; z < value.length; z++) {
            buf += String.fromCharCode(ord[<any>value.substr(z, 1)] ^ ord[<any>password.substr(j, 1)]);
            j = (j < password.length) ? j + 1 : 0
        }

        return buf
    };
}
import {User} from "../model/User";
import UserDAO from "../dao/UserDAO";

export default function registerHandler(e: JQueryEventObject): void {
    e.preventDefault();
    let userName: string = $("#register-form-username").val();
    let password: string = $("#register-form-pwd").val();
    let passwordRe: string = $("#register-form-pwd-re").val();

    if (password !== passwordRe) {
        $("#register-form-msg").text("Zadaná hesla se neshodují!");
        return;
    }

    let user = new User(userName, password);
    if (!UserDAO.saveUser(user)) {
        $("#register-form-msg").text("Uživatelské jméno již existuje!");
        return;
    }
    $("#register-form-msg").text("Registrace byla úspěšná!");
    $("#register-form-username").val("");
    $("#register-form-pwd").val("");
    $("#register-form-pwd-re").val("");
}
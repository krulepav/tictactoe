import UserDAO from "../dao/UserDAO";

export default function loginHandler(e: JQueryEventObject): void {
    e.preventDefault();
    let userName: string = $("#login-form-username").val();
    let password: string = $("#login-form-pwd").val();

    let user = UserDAO.getUser(userName, password);
    if (user === null) {
        $("#login-form-msg").text(`Uživatel ${userName} neexistuje nebo je chybé heslo!`);
        return;
    }
    if (user.password !== password) {
        $("#login-form-msg").text("Chybné heslo!");
        return;
    }
    $("#login-form-msg").text("Přihlášení bylo úspěšné!");
}
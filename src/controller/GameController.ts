import {User} from "../model/User";
import {SPAControllerHandler} from "./SPAController";
import TournamentService from "../service/TournamentService";
import TournamentDAO from "../dao/TournamentDAO";
import UserDAO from "../dao/UserDAO";

export class GameController {
    private static gameDesk: Players[][];
    private static circles: User;
    private static crosses: User;
    private static whoIsOnTurn: Players;

    @SPAControllerHandler("game")
    public static newGame() {
        let contest = TournamentService.getNextContest(TournamentDAO.getLastTournament());

        $("#players-game-login").show();
        $("#game").hide();

        $("#contest-circles-username").text(contest.usernameCircles);
        $("#contest-crosses-username").text(contest.usernameCrosses);
    }

    public static startNewGameHandler(e: JQueryEventObject) {
        e.preventDefault();

        let tournament = TournamentDAO.getLastTournament();
        let contest = TournamentService.getNextContest(tournament);

        let circles = UserDAO.getUser(contest.usernameCircles, $("#contest-circles-password").val());
        let crosses = UserDAO.getUser(contest.usernameCrosses, $("#contest-crosses-password").val());

        if (circles == null) {
            $("#contest-circles-password").val("");
        }
        if (crosses == null) {
            $("#contest-crosses-password").val("");
        }

        if (circles == null || crosses == null) {
            return;
        }

        $("#contest-circles-password").val("");
        $("#contest-crosses-password").val("");

        $("#contest-circles-game-name").text(circles.userName);
        $("#contest-crosses-game-name").text(crosses.userName);

        $("#players-game-login").hide();
        $("#game").show();

        GameController.startNewGame(circles, crosses).then((winner: User) => {
            contest.winnerUser = winner.userName;
            TournamentDAO.updateTournament(tournament);

            winner.score += 2;
            UserDAO.updateUser(winner);

            TournamentService.checkTournamentInProgress();
            alert("Vítězem se stává " + winner.userName);
            window.location.hash = "#tournament";
        }, () => {
            circles.score += 1;
            crosses.score += 1;
            UserDAO.updateUser(circles);
            UserDAO.updateUser(crosses);
            alert("Remíza :/")
            window.location.hash = "#tournament";
        });
    }

    /**
     * Starts new game and returns the winner
     * @param circles User who plays circles
     * @param crosses User who plays crosses
     * @param whoIsOnTurn Who begins - circles or crosses
     * @returns {Promise<User>} Promise which is resolved with User who won or rejected on a draw
     */
    private static startNewGame(circles: User, crosses: User, whoIsOnTurn: Players = Players.CIRCLE): Promise<User> {
        this.circles = circles;
        this.crosses = crosses;
        this.whoIsOnTurn = whoIsOnTurn;

        this.gameOver();
        $(".ttt-circle, .ttt-cross").removeClass("ttt-circle ttt-cross");
        $("#ttt-game-desk").addClass(this.whoIsOnTurn == Players.CIRCLE ? "circle-turn" : "cross-turn");
        $("#game").addClass(this.whoIsOnTurn == Players.CIRCLE ? "circle-turn" : "cross-turn");

        this.gameDesk = [];
        for (let i = 0; i < 3; i++) {
            this.gameDesk[i] = [];
            for (let j = 0; j < 3; j++) {
                this.gameDesk[i][j] = Players.NONE;
            }
        }

        return new Promise<User>((resolve, reject) => {
            let clickSound = new Audio("src/resources/click.mp3");
            $(".ttt-game-desk-bg").click((e) => {
                let clicked = $(e.target);
                let x = clicked.data("pos-x");
                let y = clicked.data("pos-y");

                if (this.gameDesk[y][x] != Players.NONE) {
                    return;
                }
                this.gameDesk[y][x] = this.whoIsOnTurn;
                clicked.addClass(this.whoIsOnTurn == Players.CIRCLE ? "ttt-circle" : "ttt-cross");
                $(".cross-turn, .circle-turn").toggleClass("cross-turn circle-turn");
                try {
                    clickSound.play();
                } catch (err) {
                    //the sound was just interrupted
                }


                if (this.checkLastPlayerWon(this.whoIsOnTurn, x, y)) {
                    this.gameOver();
                    resolve(this.currentPlayingUser());
                } else if (this.checkIsDraw()) {
                    this.gameOver();
                    reject();
                }

                this.whoIsOnTurn = this.whoIsOnTurn == Players.CIRCLE ? Players.CROSS : Players.CIRCLE;
            });
        });
    }

    private static gameOver(): void {
        $(".ttt-game-desk-bg").off("click");
        $("#ttt-game-desk").removeClass("circle-turn cross-turn");
        $("#game").removeClass("circle-turn cross-turn");
    }

    private static currentPlayingUser(): User {
        return this.whoIsOnTurn == Players.CIRCLE ? this.circles : this.crosses;
    }

    private static checkIsDraw(): boolean {
        for (let i = 0; i < 3; i++) {
            for (let j = 0; j < 3; j++) {
                if (this.gameDesk[i][j] == Players.NONE) {
                    return false;
                }
            }
        }
        return true;
    }

    private static checkLastPlayerWon(lastPlayer: Players, lastX: number, lastY: number): boolean {
        //check column
        for (let i = 0; i < 3; i++) {
            if (this.gameDesk[i][lastX] != lastPlayer) {
                break;
            } else if (i == 2) {
                return true;
            }
        }

        //check row
        for (let i = 0; i < 3; i++) {
            if (this.gameDesk[lastY][i] != lastPlayer) {
                break;
            } else if (i == 2) {
                return true;
            }
        }

        //check diagonals
        if ((lastX + lastY) % 2 == 0) {
            for (let i = 0; i < 3; i++) {
                if (this.gameDesk[i][i] != lastPlayer) {
                    break;
                } else if (i == 2) {
                    return true;
                }
            }
            for (let i = 0; i < 3; i++) {
                if (this.gameDesk[i][2 - i] != lastPlayer) {
                    break;
                } else if (i == 2) {
                    return true;
                }
            }
        }

        return false;
    }
}

enum Players {
    CIRCLE, CROSS, NONE
}
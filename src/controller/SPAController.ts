export default class SPAController {
    private static defaultPage: string;
    static controllers: { [key: string]: () => void; } = {};

    static init(defaultPage: string) {
        SPAController.defaultPage = defaultPage;
        window.addEventListener("hashchange", SPAController.onHashChange, false);
        SPAController.showPage(location.hash.replace('#', '') || defaultPage);
    }

    private static onHashChange() {
        let page = location.hash.replace('#', '');
        SPAController.showPage(page);
    }

    private static showPage(page: string) {
        if (SPAController.controllers[page])
            SPAController.controllers[page]();
        if (!page)
            page = SPAController.defaultPage;
        $('nav a').removeClass('selected');
        $(`nav a[href="#${page}"]`).addClass('selected');
        $('section[data-page]').hide();
        $(`section[data-page="${page}"]`).show();
    }
}

export function SPAControllerHandler<T>(pageName: string) {
    return function (target: any, propertyKey: string): void {
        console.log("SPA registered '" + propertyKey + "' for page hash '" + pageName + "'");
        SPAController.controllers[pageName] = target[propertyKey];
    }
}

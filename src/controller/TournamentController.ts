import {SPAControllerHandler} from "./SPAController";
import TournamentDAO from "../dao/TournamentDAO";
import ContestLayer from "../model/ContestLayer";

export default class TournamentController {

    @SPAControllerHandler("tournament")
    static showTournamentTree() {
        let tree = "";
        for (let tournament of TournamentDAO.getTournaments()) {
            tree += "<div class='tree-tournament'>";
            for (let layer of tournament.contestLayers) {
                tree += TournamentController.makeTreeRow(layer);
            }
            tree += "</div>";
        }
        $("#tournament-tree-wrapper").html(tree);
    }

    private static makeTreeRow(contestLayer: ContestLayer): string {
        let row = "<div class='tree-row'>";
        for (let contest of contestLayer.contests) {
            row +=
                '<div class="tree-contest">' +
                '<div class="tree-player' + ((contest.winnerUser == contest.usernameCircles) ? ' tree-winner' : '' ) + '">O - ' + contest.usernameCircles + '</div>' +
                '<div class="tree-player' + ((contest.winnerUser == contest.usernameCrosses) ? ' tree-winner' : '' ) + '">X - ' + contest.usernameCrosses + '</div>' +
                '</div>';
        }
        row += "</div>";
        return row;
    }
}
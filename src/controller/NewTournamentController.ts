import UserDAO from "../dao/UserDAO";
import Tournament from "../model/Tournament";
import TournamentService from "../service/TournamentService";
import {SPAControllerHandler} from "./SPAController";
import TournamentDAO from "../dao/TournamentDAO";

export default class NewTournamentController {

    @SPAControllerHandler("new-tournament")
    static showPlayersList() {
        let orderedUsers = UserDAO.getUserWrappers().sort((lhs, rhs) => {
            return lhs.score - rhs.score;
        });
        let table = "";
        for (let i = 0; i < orderedUsers.length; i++) {
            table += `<li><label><input type="checkbox" name="userName" value="${orderedUsers[i].userName}">${orderedUsers[i].userName}</label></li>`;
        }
        $("#new-tournament-players-list").html(table);
    }

    static selectAll() {
        $("#new-tournament-players-list input[type=checkbox]").prop('checked', true);
    }

    static unselectAll() {
        $("#new-tournament-players-list input[type=checkbox]").prop('checked', false);
    }

    static clearFilter() {
        $("#new-tournament-players-filter").val("");
        NewTournamentController.filterPlayerList("");
    }

    static filterPlayerList(filter: string) {
        if (filter.length > 0) {
            $(`#new-tournament-players-list input[type=checkbox]`).parent().hide();
            $(`#new-tournament-players-list input[type=checkbox][value*="${filter}"]`).parent().show();
        }
        else {
            $(`#new-tournament-players-list input[type=checkbox]`).parent().show();
        }
    }

    static onNewTournamentSubmit(e: JQueryEventObject) {
        e.preventDefault();
        let usernames: string[] = $(this).serializeArray().map((userCheckbox) => {
            return userCheckbox.value;
        });
        if (usernames.length == 0) {
            return;
        }
        if (usernames.length % 2 == 1) {
            alert("Turnaje se musí zúčastnit sudý počet hráčů");
            return;
        }
        if (confirm("Začít nový turnaj s těmito hráči?\n" + usernames.reduce((accumulated, current) => {
                    return (accumulated + "\n" + current);
                }, ""))) {
            let tournament = TournamentService.makeTournament(usernames);
            TournamentDAO.saveTournament(tournament);
            TournamentService.checkTournamentInProgress();
            location.hash = "#game";
        }
    }
}
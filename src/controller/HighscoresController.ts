import UserDAO from "../dao/UserDAO";
import {SPAControllerHandler} from "./SPAController";

export default class HighscoresController {

    @SPAControllerHandler("highscores")
    static showHighscoreTable() {
        let highscoreTableHolder = $("#highscores-table").find("> tbody");
        let orderedUsers = UserDAO.getUserWrappers().sort((lhs, rhs) => {
            return rhs.score - lhs.score;
        });
        let table = "";
        for (let i = 0; i < orderedUsers.length; i++) {
            table += `<tr><td>${i + 1}</td><td>${orderedUsers[i].userName}</td><td>${orderedUsers[i].score}</td></tr>`;
        }
        highscoreTableHolder.html(table);
    }
}
